<?php

namespace ProdCalcFramework\Exception;

use ProdCalcFramework\Exception\BasicException;

class InternalServerErrorException extends BasicException
{
}
