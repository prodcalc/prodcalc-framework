<?php

namespace ProdCalcFramework\Exception\Http;

use ProdCalcFramework\Exception\BasicException;

class InvalidJsonException extends BasicException {
}
