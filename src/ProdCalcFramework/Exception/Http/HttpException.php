<?php

namespace ProdCalcFramework\Exception\Http;

use ProdCalcFramework\Exception\BasicException;

class HttpException extends BasicException {
}
