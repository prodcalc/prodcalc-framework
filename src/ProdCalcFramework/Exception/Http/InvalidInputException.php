<?php

namespace ProdCalcFramework\Exception\Http;

use ProdCalcFramework\Exception\BasicException;

class InvalidInputException extends BasicException {
}
