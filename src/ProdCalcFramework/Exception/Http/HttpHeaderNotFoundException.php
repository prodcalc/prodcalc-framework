<?php

namespace ProdCalcFramework\Exception\Http;

use ProdCalcFramework\Exception\BasicException;

class HttpHeaderNotFoundException extends BasicException {
}
