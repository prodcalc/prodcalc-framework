<?php

namespace ProdCalcFramework\Exception\Db;

use ProdCalcFramework\Exception\BasicException;

class NoRecordFoundException extends BasicException {
}
