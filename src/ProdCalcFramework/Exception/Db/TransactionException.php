<?php

namespace ProdCalcFramework\Exception\Db;

use ProdCalcFramework\Exception\BasicException;

class TransactionException extends BasicException {
}
