<?php

namespace ProdCalcFramework\Exception\Db;

use ProdCalcFramework\Exception\BasicException;

class MultipleRowsException extends BasicException
{
}
