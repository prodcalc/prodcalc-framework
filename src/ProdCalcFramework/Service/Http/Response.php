<?php

namespace ProdCalcFramework\Service\Http;

class Response
{
    private $httpStatus;

    private $headers	= [];
    private $body		= '{}';

    public function getHttpStatus()
    {
        return $this->httpStatus;
    }

    public function setHttpStatus($httpStatusCode)
    {
        $this->httpStatus = $httpStatusCode;

        return $this;
    }

    public function getBody()
    {
        return $this->body;
    }

    public function setBody($body)
    {
        $this->body = $body;

        return $this;
    }

    public function getHeaders()
    {
        return $this->headers;
    }

    public function setHeaders($headers)
    {
        $this->headers = $headers;

        return $this;
    }
}
