<?php

namespace ProdCalcFramework\Service\Http;

use ProdCalcFramework\Service\BasicService;
use ProdCalcFramework\Service\Http\Request;
use ProdCalcFramework\Service\Http\Response;

use ProdCalcFramework\Exception\Http\HttpException;

use GuzzleHttp\Client;
use GuzzleHttp\Exception\TransferException;

class HttpService extends BasicService
{
    private $request;
    private $response;
    private $httpClient;

    public function get()
    {
        $t = $this;

        $t->fixHost();  // necessary to prevent 404 due to vhost use

        $req = $t->getRequest();
        $res = $t->getResponse();

        try {
            $cr    = $t

                ->httpClient
                ->request(

                    'GET',

                    $req->getEndpoint(),

                    [

                        'headers' => $req->getHeaders(),

                        'query' => is_null($req->getBody()) ? [] : $req->getBody()

                    ]
                );

            $res->setHttpStatus($cr->getStatusCode());

            $res->setHeaders($cr->getHeaders());

            $res->setBody(\json_decode($cr->getBody(), true));

            return $this;
        } catch (TransferException $ex) {

            throw new HttpException($ex->getMessage());
        }
    }

    public function post()
    {
        $t = $this;

        $t->fixHost();  // necessary to prevent 404 due to vhost use

        $req = $t->getRequest();
        $res = $t->getResponse();

        try {

            $cr    = $t

                ->httpClient
                ->request(

                    'POST',

                    $req->getEndpoint(),

                    [

                        'headers' => $req->getHeaders(),

                        'json' => is_null($req->getBody()) ? '{}' : $req->getBody()

                    ]
                );

            $res->setHttpStatus($cr->getStatusCode());

            $res->setHeaders($cr->getHeaders());

            $res->setBody(\json_decode($cr->getBody(), true));

            return $this;
        } catch (TransferException $ex) {

            throw new HttpException($ex->getMessage());
        }
    }

    private function fixHost()
    {

        $t = $this;

        $headers = $t

            ->getRequest()
            ->getHeaders();

        $headers['Host'] = explode(

            '/',

            $t

                ->getRequest()
                ->getEndpoint()

        )[2];

        $t

            ->getRequest()
            ->setHeaders($headers);
    }

    public function getRequest()
    {
        return $this->request;
    }

    public function getResponse()
    {
        return $this->response;
    }

    public function __construct()
    {
        $t = $this;

        $t->request = new Request();
        $t->response = new Response();

        $t->httpClient    = new Client([

            'http_errors' => false

        ]);
    }
}
