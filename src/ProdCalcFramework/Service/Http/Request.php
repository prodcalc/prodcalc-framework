<?php

namespace ProdCalcFramework\Service\Http;

class Request
{
    private $endPoint;
    private $headers = [];
    private $body = null;

    public function setEndpoint($endPoint)
    {
        $this->endPoint = $endPoint;

        return $this;
    }

    public function getEndpoint()
    {
        return $this->endPoint;
    }

    public function setHeaders($headers)
    {
        $this->headers = $headers;

        return $this;
    }

    public function getHeaders()
    {
        return $this->headers;
    }

    public function setBody($body)
    {
        $this->body = $body;

        return $this;
    }

    public function getBody()
    {
        return $this->body;
    }
}
