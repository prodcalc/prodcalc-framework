<?php

namespace ProdCalcFramework\Endpoint;

use ProdCalcFramework\Exception\Http\InvalidInputException;

class Errors
{

	private $errorList	= [];

	function addError(

		$errorMessage

	) {

		$t = $this;

		if (

			in_array(

				$errorMessage,

				$t->getErrorList()

			)

		) {

			return;
		}

		$this->errorList[]	= $errorMessage;

		return $t;
	}

	/*@deprecated */
	function add($errorMessage)
	{

		$t = $this;

		if (

			in_array(

				$errorMessage,

				$t->getErrorList()

			)

		) {

			return;
		}

		$this->errorList[]	= $errorMessage;

		return $t;
	}

	function setErrorList(

		$errorList

	) {

		$t = $this;

		$t->errorList = $errorList;

		return $t;
	}

	function getErrorList()
	{

		$t = $this;

		return $t->errorList;
	}

	/*@deprecated*/
	function validate($regExp, &$value, $message)
	{

		$t = $this;

		if (!isset($value) || !preg_match($regExp, $value)) {

			$this->add($message);
		}

		return $t;
	}

	function throwInvalidInputException()
	{

		$t = $this;

		if (

			count($t->errorList) > 0

		) {

			throw new InvalidInputException();
		}
	}
}
