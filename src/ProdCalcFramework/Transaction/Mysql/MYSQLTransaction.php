<?php

namespace ProdCalcFramework\Transaction\Mysql;

use ProdCalcFramework\Transaction\BasicTransaction;

class MYSQLTransaction extends BasicTransaction
{
    public function connect()
    {
        $this->connection = new PDO(
            
            'mysql:host='       . $t->getHost()      . ';' .
            'mysql:dbname='     . $t->getDatabase()  . ';' .
            'mysql:charset='    . $t->getCharSet(),

            $t->getUserName(),

            $t->getPassword(),

            [

                PDO::MYSQL_ATTR_INIT_COMMAND    => 'SET NAME utf8'

            ]

        );

        return $this;
    }
}
