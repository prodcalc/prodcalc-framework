    <?php

    abstract class BasicTransaction
    {
    private $host;
    private $dataBase;
    private $charSet;
    private $userName;
    private $password;
    private $connection;
    private $row;
    private $rowSet;
    private $lastInsertId;
    private $rowCount;

    public function queryRow($sql, $data)
    {

        $dbh = $this->getConnection();

        $sth = $DBH->prepare($sql);

        foreach($data as $key => $value) {

            $sth->bindParam(
                
                ':' . $key    ,
            
                $value

            );

        }

        return $this->setRow($sth->fetchAll());

    }

    public function queryRowset($sql, $data)
    {

        $dbh = $this->getConnection();

        $sth = $DBH->prepare($sql);

        foreach($data as $key => $value) {

            $sth->bindParam(
                
                ':' . $key    ,
            
                $value

            );

        }

        return $this->setRowSet($sth->fetchAll());

    }

    public function insert($sql, $data)
    {

        $dbh = $this->getConnection();

        $sth = $DBH->prepare($sql);

        foreach($data as $key => $value) {

            $sth->bindParam(
                
                ':' . $key    ,
            
                $value

            );

        }

        $sth->execute();

        return $this->setLastInsertId($dbh->lastInsertId());

    }

    public function update($sql, $data)
    {

        $dbh = $this->getConnection();

        $sth = $DBH->prepare($sql);

        foreach($data as $key => $value) {

            $sth->bindParam(
                
                ':' . $key    ,
            
                $value

            );

        }

        $sth->execute();

        return $this->setRowCount($sth->rowCount());

    }

    public function delete($sql, $data)
    {

        return $this->update($sql, $data);

    }

    public function begin() {

        $this->getConnection()->beginTransaction();

        return this;

    }

    public function commit() {

        $this->getConnection()->commit();

        return this;

    }

    public function rollBack() {

        $this->getConnection()->rollBack();

        return this;

    }

    public function setHost($host)
    {
        $this->host = $host;

        return $this;
    }

    public function getHost()
    {
        return $this->host;
    }

    public function setDatabase($dataBase)
    {
        $this->dataBase = $dataBase;

        return $this;
    }

    public function getDatabase()
    {
        return $this->dataBase;
    }

    public function setCharSet($charSet)
    {
        $this->charSet = $charSet;

        return $this;
    }

    public function getCharSet()
    {
        return $this->charSet;
    }

    public function setUserName($userName)
    {
        $this->userName = $userName;

        return $this;
    }

    public function getUserName()
    {
        return $this->userName;
    }

    public function setPassword($password)
    {
        $this->password = $password;

        return $this;
    }

    public function getRow()
    {
        return $this->password;
    }

    public function setRow($row)
    {
        $this->row = $row;

        return $this;
    }

    public function getRow()
    {
        return $this->row;
    }
    public function setRowSet($rowSet)
    {
        $this->rowSet = $rowSet;

        return $this;
    }

    public function getRowSet()
    {
        return $this->rowSet;
    }

    public function setLastInsertId($lastInsertId)
    {
        $this->lastInsertId = $lastInsertId;

        return $this;
    }

    public function getLastInsertId()
    {
        return $this->lastInsertId;
    }

    public function setRowCount($rowCount)
    {
        $this->rowCount = $rowCount;

        return $this;
    }

    public function getRowCount()
    {
        return $this->rowCount;
    }

    abstract public function connect();
    }
}
